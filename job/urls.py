from django.urls import path, include

from rest_framework.routers import DefaultRouter
from job import views

router = DefaultRouter()
router.register('job-viewset', views.JobViewSets, basename='job-viewset')
router.register('apply-job-viewset', views.ApplyViewset, basename='apply-job-viewset')
router.register('check-status', views.StatusView, basename='check-status-viewset')

urlpatterns = [
    path('', include(router.urls)),
    path('status/<int:pk>/', views.StatusAPI.as_view({'get': 'get'}))
]