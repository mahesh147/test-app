from rest_framework import permissions

SAFE_METHODS = ['GET', 'HEAD', 'OPTIONS']




class EditJobDetails(permissions.BasePermission):
    """
    Allow users to edit their own job description
    """
    def has_object_permission(self, request, view, obj):
        """
        Check user is trying to edit their own job description
        """
        print(obj.id, request.user.id)
        return obj.id == request.user.id


class IsAuthenticatedOrReadOnly(permissions.BasePermission):
    """
    The request is authenticated as a user, or is a read-only request.
    """

    def has_permission(self, request, view):
        if (request.method in SAFE_METHODS or
            request.user and
            request.user.is_authenticated
            and request.user.is_staff):
            return True
        return False
