from django.shortcuts import render


from rest_framework.views import APIView
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.authentication import TokenAuthentication
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.settings import api_settings
from rest_framework import generics
from rest_framework.permissions import AllowAny

from rest_framework.permissions import IsAuthenticated

from job import serializers
from job.permissions import EditJobDetails, IsAuthenticatedOrReadOnly
from job import models



class JobViewSets(viewsets.ViewSet):


    serializer_class = serializers.JobSerializers

    permission_classes = [IsAuthenticatedOrReadOnly]

    
    def list(self, request):
        """
        Return a jobs
        """
        permission_classes = [AllowAny]
        queryset = models.Job.objects.all()
        serializer = self.serializer_class(queryset, many=True)
        return Response(serializer.data)


    def create(self, request):
        """
        Create a new job
        """
        permission_classes = [IsAuthenticated]
        serializer = self.serializer_class(data=request.data, context={'user':request.user})

        if serializer.is_valid():
            serializer.save()
            message = f'Successfully saved the object'
            return Response({'message':message})
        else:
            return Response(
                serializer.errors
            )


    def partial_update(self, request, pk=None):


        instance = models.Job.objects.get(id=pk)
        serializer = self.serializer_class(instance, data=request.data, context={'user':request.user}, partial=True)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response({'message':'Updated the Job'})


    def delete(self, request, pk=None):
        instance = models.Job.objects.get(id=pk)
        if instance.hr_user == request.user:
            instance.delete()

            return Response({'message':'Job posting deleted'})
        else:
            return Response({'message':'[ERROR]Not Permitted'})



class ApplyViewset(viewsets.ViewSet):

    serializer_class = serializers.ApplyJobSerializers
    permission_classes = [AllowAny]

    def partial_update(self, request, pk=None):
        print(f'pk:{pk}')
        job = models.Job.objects.get(id=pk)
        print(f'job : {job}')
        if job == []:
            return Response({'message':'Job does not exist'})
        print(request.data['email'])
        try:
            applicant_instance = models.Applicant.objects.get(email=request.data['email'])
        except:
            applicant_instance = models.Applicant.objects.create(name=request.data['name'], email=request.data['email'])

        serializer = self.serializer_class(applicant_instance, data=request.data, context={'pk':pk}, partial=True)
        print(f'serializer back in view : {serializer}')
        if serializer.is_valid():
            serializer.save()

            message = f'Successfully Added the applicant.'
            status_url = f'127.0.0.1:8000/job/status/{applicant_instance.id}'
            return Response({'message':message, 'status_url':status_url})
        else:
            return Response(
                serializer.errors
            )


    def list(self, request):
        """
        Return a applicants
        """
        permission_classes = [AllowAny]
        queryset = models.Applicant.objects.all()
        serializer = self.serializer_class(queryset, many=True)
        return Response(serializer.data)


class StatusView(viewsets.ViewSet):

    serializer_class = serializers.StatusJobSerializers
    permission_classes = [AllowAny]

    def list(self, request, pk=None):
        return Response({'message':'hello'})

class StatusAPI(viewsets.ViewSet):
    serializer_class = serializers.StatusJobSerializers
    permission_classes = [AllowAny]

    def get(self, request, pk=None):
        queryset = models.Applicant.objects.get(id=pk)
        status = queryset.job_status
        return Response({'job_status': status})