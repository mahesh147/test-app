from django.contrib import admin

from job import models
# Register your models here.
admin.site.register(models.Job)
admin.site.register(models.Applicant)