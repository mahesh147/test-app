from django.db import models

from hr.models import HRUser

# Create your models here.


class Job(models.Model):
    hr_user = models.ForeignKey(
        HRUser,
        on_delete = models.CASCADE
        )
    job_title = models.CharField(max_length=255)
    job_openings = models.IntegerField(null=False)
    job_location = models.CharField(max_length=255)
    created_on = models.DateField(auto_now_add=True)

    def __str__(self):
        return self.job_title


class Applicant(models.Model):
    EDUCATION = (
        ('12th', ('12th grade')),
        ('bachelor', ('Bachelor Degree')),
        ('master', ('Master Degree')),
        ('phd', ('PHd Degree'))
    )


    name = models.CharField(max_length=255)
    email = models.EmailField(unique=True)
    birthdate = models.DateField(null=True)
    education = models.CharField(max_length=10,  choices=EDUCATION, null=True)
    job_applied = models.ForeignKey(Job, on_delete=models.CASCADE, null=True)
    job_status = models.BooleanField(default=False)

    def __str__(self):
        return self.email

