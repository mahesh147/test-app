from rest_framework import serializers


from job import models


class JobSerializers(serializers.ModelSerializer):

        class Meta:
            model = models.Job
            fields = ('id', 'job_title', 'job_openings', 'job_location')



        def create(self, validated_data):
            """
            Create a new job
            """

            print(f'request object in serializer {self.context}')

            job = models.Job.objects.create(
                hr_user = self.context['user'],
                job_title = validated_data['job_title'],
                job_openings = validated_data['job_openings'],
                job_location = validated_data['job_location']
            )

            return job

        def update(self, instance, validated_data):
            print(f"instance.hr_user : {instance.hr_user}, self.context['user'] : {self.context['user']}")
            if instance.hr_user == self.context['user']:
                print(f"instance.hr_user : {instance.hr_user}, self.context['user'] : {self.context['user']}")
                instance.job_title = validated_data.get('job_title', instance.job_title)
                instance.job_openings = validated_data.get('job_openings', instance.job_openings)
                instance.job_location = validated_data.get('job_location', instance.job_location)
                instance.save()
                return instance
            else:
                return 'Error'


class ApplyJobSerializers(serializers.ModelSerializer):

    class Meta:
        model = models.Applicant
        fields = ('id', 'name', 'email', 'education', 'birthdate', 'job_applied')


    def update(self, instance, validated_data):
        job = models.Job.objects.get(id=self.context['pk'])
        if job == []:
            return 'Job does not exist'
        print(f'')
        print(f'instance.job_applied : {instance.job_applied}')
        if instance.job_applied != job:
            instance.job_applied = job
            instance.name = validated_data.get('name')
            instance.email = validated_data.get('email')
            instance.education = validated_data.get('education')
            instance.birthdate = validated_data.get('birthdate')
            print(f"instance.job_applied : {instance.job_applied} | {job} Applicant has not applied yet for the job")
            print(f'After applying changes : {instance.job_applied}')
            instance.save()
            return instance
        else:
            print(f"job_applied : {instance.job_applied} | {job} Applicant has already applied for this job")
            return 'You have already applied for this job'

class StatusJobSerializers(serializers.ModelSerializer):

    class Meta:
        model = models.Applicant
        fields = ('id', 'job_applied', 'job_status')


