# Generated by Django 3.1.2 on 2020-11-05 04:17

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('job', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='job',
            name='applicant',
        ),
        migrations.AddField(
            model_name='applicant',
            name='jobs_applied',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='job.job'),
            preserve_default=False,
        ),
    ]
