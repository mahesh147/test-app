# Generated by Django 3.1.2 on 2020-11-05 06:04

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('job', '0004_auto_20201105_0438'),
    ]

    operations = [
        migrations.AddField(
            model_name='applicant',
            name='job_status',
            field=models.BooleanField(default=False),
        ),
    ]
