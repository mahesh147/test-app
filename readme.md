# Test App

## Endpoints

### HR

- Create HR Account :  

```
[POST] 127.0.0.1:8000/hr/register/
```

Pass email, name and password in the body as x-www-forms-urlencoded

Note: By default, HR cannot post, update, or delete any jobs, since admin (superuser) needs to approve it. This permission is set using the is_staff attribute (which is set to False).

- Login as HR:

  ```
  [POST] 127.0.0.1:8000/api/token/
  ```

  - Pass email, and password in the body as x-www-forms-urlencoded

  Returns refresh and access tokens. This is the default behaviour of JWT. The endpoints are set using the JWT's documentation

- Create a job:

  ```
  [POST] 127.0.0.1:8000/job/job-viewset/
  ```

  - Pass job_title, job_openings, and job_location into the body. Requires Authentication. Pass access token as Bearer Token in Authorisation

- Retrieve all jobs:

  ```
  [GET] 127.0.0.1:8000/job/job-viewset/
  ```

  Returns all of the available jobs from the database (Does not require authentication)

- Update a job:

  ```
  [PATCH] 127.0.0.1:8000/job/job-viewset/<int:job_id>/
  ```

  Pass either (or all) job_title, job_openings, or job_location to change. Requires Authentications. 

  job_id -> Id of the job object to change. You can get the job_id for all objects from [GET] job/job-viewset/

- Delete a job:

  ```
  [DELETE] 127.0.0.1:8000/job/job-viewset/<int:job_id>/
  ```

  Requires authentications. Deletes the job posting from the server.

  - See all applicants:

    ```
    [GET] 127.0.0.1:8000/job/apply-job-viewset/
    ```

    Returns all the applicant's id and their corresponding job's id as well

  - See applicant only for a particular HR's job posting:

  ```
  [GET] 127.0.0.1:8000/hr/shortlist-reject/
  ```

  ​	Requires authentication. Retrieves the applicant's id and their current shortlist/rejection status

- Shortlist or Reject an applicant:

```
[POST] 127.0.0.1:8000/hr/shortlist-reject/<int:applicant_id>/
```

Requires Authentication. Pass in the job_status as a Boolean value indicating whether the candidate got shortlisted or rejected

### Candidate

- Apply for a job:

  ```
  [POST] 127.0.0.1:8000/job/apply-job-viewset/<int:job_id>/
  ```

  Pass in the candidate information in the body. Requires name, and email. Other fields are set to optional for now.

- Check status of job:

  ```
  [POST] 127.0.0.1:8000/job/status/<int:job_id>/
  ```

  Returns a Boolean value indicating whether the candidate has been shortlisted or not 