from rest_framework import serializers
from django.core.mail import send_mail

from hr import models

from job.models import Job, Applicant

class HRRegisterSerializers(serializers.ModelSerializer):
    """
    Serializes a HR register object
    """

    class Meta:
        model = models.HRUser
        fields = ('id', 'email', 'name', 'password')
        extra_kwargs = {
            'password': {
                'write_only': True,
                'style': {'input_type' : 'password'}
            }
        }

    def create(self, validated_data):
        """
        Create an return a new user
        """
        user = models.HRUser.objects.create_user(
            email=validated_data['email'],
            name=validated_data['name'],
            password=validated_data['password']
            )

        return user
    
    def update(self, instance, validated_data):
        """Handle updating user account"""
        if 'password' in validated_data:
            password = validated_data.pop('password')
            instance.set_password(password)

        return super().update(instance, validated_data)


class HRLogin(serializers.ModelSerializer):
    class Meta:
        model = models.HRUser
        fields = ('id', 'email', 'password')
        extra_kwargs = {
            'password': {
                'write_only': True,
                'style': {'input_type' : 'password'}
            }
        }

    def create(self, validated_data):

        user = models.HRUser.objects.get(email=validated_data['email'])

        if user.check_password(validated_data['password']):
            return user



class ShorlistOrRejectSerializers(serializers.ModelSerializer):

    class Meta:
        model = Applicant
        fields = ('id','job_status')

    def update(self, instance, validated_data):
        status = bool(validated_data.get('job_status'))
        if status:
            try:
                send_mail(
                'You have been shortlisted!',
                'You have been shortlisted for the job',
                'mahesh@zappyhire.com',
                [instance.email],
        fail_silently=False,
)
            except Exception  as e:
                print(e)
        else:
            try:
                send_mail(
                'You have not benn shortlisted!',
                'You have not been shortlisted for the job',
                'mahesh@zappyhire.com',
                [instance.email],
        fail_silently=False,
            )
            except Exception as e:
                print(e)
        instance.job_status = bool(validated_data.get('job_status'))
        instance.save()
        return instance