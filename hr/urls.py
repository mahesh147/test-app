from django.urls import path, include
from rest_framework.routers import DefaultRouter
from hr import views

router = DefaultRouter()
router.register('shortlist-reject', views.ShortlistOrReject, basename='shortlist-reject')

urlpatterns = [
    path('register/', views.HRCreateAPIView.as_view()),
    path('', include(router.urls))
]
