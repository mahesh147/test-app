from django.shortcuts import render, HttpResponse
from django.core.mail import send_mail


from rest_framework.views import APIView
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.authentication import TokenAuthentication
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.settings import api_settings
from rest_framework import generics
from rest_framework.permissions import AllowAny
from rest_framework.permissions import IsAuthenticated
from hr import serializers
from hr import permissions
from hr import models
from job.models import Job, Applicant

# Create your views here.

def hello(request):
    return HttpResponse('hello')

class HRCreateAPIView(generics.CreateAPIView):
    queryset = models.HRUser.objects.all()
    serializer_class = serializers.HRRegisterSerializers
    permission_classes = (AllowAny,)    


class ShortlistOrReject(viewsets.ViewSet):

    permission_classes = [IsAuthenticated, permissions.IsAdminUser]
    serializer_class = serializers.ShorlistOrRejectSerializers

    def list(self, request):
        queryset = Job.objects.get(hr_user=request.user)
        applicant = Applicant.objects.get(job_applied=queryset)

        return Response({'Job':queryset.job_title, 'Applicant':applicant.name, 'job_id':queryset.id, 
        'applicant_id':applicant.id, 'status':applicant.job_status})


    def partial_update(self, request, pk=None):
        print(f'pk:{pk}')
        applicant = Applicant.objects.get(id=pk)
        serializers = self.serializer_class(applicant, request.data)
        if serializers.is_valid():
            message = "Successfully updated the applicant's job status"
            serializers.save()
            return Response({'message':message})
        else:
            return Response(
                serializers.errors
            )
